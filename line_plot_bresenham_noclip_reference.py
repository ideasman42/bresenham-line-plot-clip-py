# Licensed: Apache 2.0

# Keeping for reference, however this gives *slightly*
# different gradient from the clipping version (even when not clipping)
# so only keeping it for reference.

from typing import (
    Tuple,
    Callable,
)

def plot_line_v2v2i(
        p1: Tuple[int, int],
        p2: Tuple[int, int],
        callback: Callable[[int, int], None],
) -> None:
    x1, y1 = p1
    x2, y2 = p2

    # if x1 == x2 or y1 == y2, then it does not matter what we set here
    if x2 > x1:
        x_sign = 1
        delta_x = (x2 - x1) * 2
    else:
        x_sign = -1
        delta_x = (x1 - x2) * 2

    if y2 > y1:
        y_sign = 1
        delta_y = (y2 - y1) * 2
    else:
        y_sign = -1
        delta_y = (y1 - y2) * 2

    callback(x1, y1)

    if delta_x >= delta_y:
        # error may go below zero
        error = delta_y - (delta_x // 2)

        while x1 != x2:
            if error >= 0:
                if error != 0 or (x_sign > 0):
                    y1 += y_sign
                    error -= delta_x
                # else do nothing
            # else do nothing

            x1 += x_sign
            error += delta_y

            callback(x1, y1)
    else:
        # error may go below zero
        error = delta_x - (delta_y // 2)

        while y1 != y2:
            if error >= 0:
                if error != 0 or (y_sign > 0):
                    x1 += x_sign
                    error -= delta_y
                # else do nothing
            # else do nothing

            y1 += y_sign
            error += delta_x

            callback(x1, y1)
