
Bresenham's Line Drawing Algorithm with Clipping Support
========================================================

This is a small Python implementation of:

   Yevgeny P. Kuzmin. Bresenham's Line Generation Algorithm with
   Built-in Clipping. Computer Graphics Forum, 14(5):275--280, 2005.

With tests to verify that the clipped version matches the result of the same code without clipping.

- See: ``line_plot_bresenham_clip.py``.
- To test, run: ``test_plot.py``.

Changes from Original
---------------------

- Points are plotted in the direction defined by input points (from ``p1`` to ``p2``).
- In some cases there was a subtle difference in the line where, rotating of flipping the points,
  calculating the line, then transforming back would give slightly different results.
  The asymmetry was caused by the code swapping the X and Y axis to avoid code duplication.
