
# Licensed: Apache 2.0

# Reference for the paper:
# Bresenham's Line Generation Algorithm with Built-in Clipping
# by: Kuzmin, Yevgeny P.

from typing import (
    Callable,
    Tuple,
)

def plot_line_v2v2i(
    p1: Tuple[int, int],
    p2: Tuple[int, int],
    callback: Callable[[int, int], None],
) -> None:
    x1, y1 = p1
    x2, y2 = p2

    del p1, p2

    # Vertical line
    if x1 == x2:
        if y1 <= y2:
            for y in range(y1, y2 + 1):
                callback(x1, y)
        else:
            for y in range(y1, y2 - 1, -1):
                callback(x1, y)
        return

    # Horizontal line
    if y1 == y2:
        if x1 <= x2:
            for x in range(x1, x2 + 1):
                callback(x, y1)
        else:
            for x in range(x1, x2 - 1, -1):
                callback(x, y1)
        return

    # Now simple cases are handled, perform Bresenham's calculation
    if x1 < x2:
        sign_x = 1
    else:
        sign_x = -1

        # Invert sign, invert again right before plotting.
        x1 = -x1
        x2 = -x2

    if y1 < y2:
        sign_y = 1
    else:
        sign_y = -1

        # Invert sign, invert again right before plotting.
        y1 = -y1
        y2 = -y2

    delta_x = x2 - x1
    delta_y = y2 - y1

    delta_x_step = 2 * delta_x
    delta_y_step = 2 * delta_y

    # Plotting values
    x_pos = x1
    y_pos = y1

    if delta_x >= delta_y:
        error = delta_y_step - delta_x
        x_pos_end = x2 + 1
        if sign_y == -1:
            y_pos = -y_pos
        if sign_x == -1:
            x_pos = -x_pos
            x_pos_end = -x_pos_end
        delta_x_step -= delta_y_step

        while x_pos != x_pos_end:
            callback(x_pos, y_pos)

            if error >= 0:
                y_pos += sign_y
                error -= delta_x_step
            else:
                error += delta_y_step

            x_pos += sign_x
    else:
        error = delta_x_step - delta_y
        y_pos_end = y2 + 1
        if sign_x == -1:
            x_pos = -x_pos
        if sign_y == -1:
            y_pos = -y_pos
            y_pos_end = -y_pos_end
        delta_y_step -= delta_x_step

        while y_pos != y_pos_end:
            callback(x_pos, y_pos)

            if error >= 0:
                x_pos += sign_x
                error -= delta_y_step
            else:
                error += delta_x_step

            y_pos += sign_y
