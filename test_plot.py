# Licensed: Apache 2.0

import line_plot_bresenham_clip
import line_plot_bresenham_noclip

import random

from typing import (
    Tuple,
)

TEST_NUMBER = 10000

RANGE = (0, 200)
CLIPPING_XMIN = 50
CLIPPING_YMIN = 50
CLIPPING_XMAX = 150
CLIPPING_YMAX = 150

# for debugging tests!
# random.seed(26)


def swap_axis(p: Tuple[int, int]) -> Tuple[int, int]:
    return p[1], p[0]


def main() -> None:
    tests = 0
    tests_empty = 0

    for i in range(TEST_NUMBER):
        p1 = (random.randint(*RANGE), random.randint(*RANGE))
        p2 = (random.randint(*RANGE), random.randint(*RANGE))

        points_noclip = []
        points_pre_clip = []
        points_pre_clip_swap = []

        def callback_noclip(x: int, y: int) -> None:
            # non clipping version, we clip here
            if not (((x >= CLIPPING_XMIN) and (x <= CLIPPING_XMAX) and
                     (y >= CLIPPING_YMIN) and (y <= CLIPPING_YMAX))):
                return
            points_noclip.append((x, y))

        def callback_pre_clipped(x: int, y: int) -> None:
            assert((x >= CLIPPING_XMIN) and (x <= CLIPPING_XMAX) and
                   (y >= CLIPPING_YMIN) and (y <= CLIPPING_YMAX))
            points_pre_clip.append((x, y))

        def callback_pre_clipped_swap(x: int, y: int) -> None:
            x, y = swap_axis((x, y))
            # non clipping version, we clip here
            if not (((x >= CLIPPING_XMIN) and (x <= CLIPPING_XMAX) and
                     (y >= CLIPPING_YMIN) and (y <= CLIPPING_YMAX))):
                return
            points_pre_clip_swap.append((x, y))

        line_plot_bresenham_noclip.plot_line_v2v2i(
            p1, p2, callback_noclip,
        )

        line_plot_bresenham_clip.plot_line_v2v2i(
            p1, p2, callback_pre_clipped,
            CLIPPING_XMIN,
            CLIPPING_YMIN,
            CLIPPING_XMAX,
            CLIPPING_YMAX,
        )

        line_plot_bresenham_clip.plot_line_v2v2i(
            swap_axis(p1), swap_axis(p2), callback_pre_clipped_swap,
            CLIPPING_XMIN,
            CLIPPING_YMIN,
            CLIPPING_XMAX,
            CLIPPING_YMAX,
        )

        if points_noclip != points_pre_clip:
            print("Found error drawing between points (normal):", p1, p2)
            print(points_noclip)
            print(points_pre_clip)
            assert(tuple(points_noclip) == tuple(points_pre_clip))

        if points_noclip != points_pre_clip_swap:
            print("Found error drawing between points (sym):", p1, p2)
            print(points_noclip)
            print(points_pre_clip_swap)
            assert(tuple(points_noclip) == tuple(points_pre_clip_swap))

        tests += 1
        if not points_noclip:
            tests_empty += 1

    print("Finished testing:", tests, tests_empty, "were entirely outside the clipping range")

if __name__ == "__main__":
    main()
